<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Building a Linux image</a>
<ul>
<li><a href="#sec-1-1">1.1. Setting up the project</a></li>
<li><a href="#sec-1-2">1.2. Base system</a></li>
<li><a href="#sec-1-3">1.3. Image contents</a>
<ul>
<li><a href="#sec-1-3-1">1.3.1. A sysroot</a></li>
<li><a href="#sec-1-3-2">1.3.2. An initramfs</a></li>
<li><a href="#sec-1-3-3">1.3.3. A Linux kernel</a></li>
<li><a href="#sec-1-3-4">1.3.4. Project-specific elements</a></li>
</ul>
</li>
<li><a href="#sec-1-4">1.4. Creating the image</a></li>
<li><a href="#sec-1-5">1.5. Building the image</a></li>
</ul>
</li>
</ul>
</div>
</div>

# Building a Linux image<a id="sec-1" name="sec-1"></a>

## Setting up the project<a id="sec-1-1" name="sec-1-1"></a>

To create an image, we will want to use the x86image plugin from
the `bst-external` repository. The `bst-external` repository is a
collection of plugins that are either too niche or unpolished to
include as plugins in the main repository, but are useful for
various purposes.

Install the latest version of this repository:

    git clone https://gitlab.com/BuildStream/bst-external.git
    cd bst-external
    ./setup.py install

This should make bst-external plugins available to buildstream. To
use the x86image and docker plugins in our project, we need to set
up plugins in our `project.conf`:

    # project.conf

    name: test-image
    element-path: elements

    # Allow the project use any version higher than 0 of the x86image
    # plugin installed through pip from the bst-external package
    plugins:
      - origin: pip
        package-name: BuildStream-external
        elements:
          x86image: 0
      - origin: pip
        package-name: docker
        elements:
          docker: 0

We also set aliases for all project pages we will be fetching
sources from:

    aliases:
      kernel: https://www.kernel.org/pub/
      gnu: http://ftp.gnu.org/gnu/
      busybox: https://www.busybox.net/downloads/

## Base system<a id="sec-1-2" name="sec-1-2"></a>

The base system will be used to **build** the image and the project,
but it won't be a part of the final result. It should contain
everything that is required to build both the project and the
tools required to create an image.

The x86image plugin requires a specific set of tools to create an
image. To make using this plugin easier, we provide an
alpine-based base system using docker that contains all required
tools:

    # elements/base.bst

    kind: import
    sources:
    - kind: tar
      url: file:///src/files/image-tools.tar
      base-dir: '*'
      ref: b44be0ee60ad40e0130266ceea9ce64bcce2deea4cb111f7f66ad4ba05301125
      # - kind: buildstream-external:gitlocal
      #   url: https://registry.hub.docker.com/library/buildstream/image-tools/
      #   track: latest

This image only provides musl-libc and busybox for building,
should your project require GNU tools it will be simpler to create
your own and ensure that your base system includes the following
packages:

-   parted
-   mtools
-   e2fsprogs
-   dosfstools
-   syslinux
-   nasm
-   autoconf
-   gcc
-   g++
-   make
-   gawk
-   bc
-   linux-headers

## Image contents<a id="sec-1-3" name="sec-1-3"></a>

The final linux image will consist of several elements that can be
broadly summarized in four scopes:

1.  A sysroot
2.  An initramfs
3.  A Linux kernel
4.  Project-specific elements

### A sysroot<a id="sec-1-3-1" name="sec-1-3-1"></a>

Before we create the elements to make an actual image, we will set
up the sysroot that will form the user space. For this tutorial,
we will create a very simple system.

The sysroot must contain everything required to run a Linux
system - this will usually be [GNU coreutils](https://www.gnu.org/software/coreutils/coreutils.html) or [BusyBox](https://www.busybox.net/about.html) - and any
runtime dependencies, if these tools are not statically linked -
often [glibc](https://www.gnu.org/software/libc/) or [musl](https://www.musl-libc.org/).

For this tutorial we will build a BusyBox + musl system:

    kind: stack
    description: The image contents
    depends:
      - contents/busybox.bst

A keen eye may notice that this does not include a `musl.bst`
dependency - this is because the busybox element itself is set to
run-depend on musl, which we use later to stage sysroot content,
but not the base system.

For the specifics of the included elements, refer to the
accompanying project repository.

### An initramfs<a id="sec-1-3-2" name="sec-1-3-2"></a>

Now that we have defined the basic sysroot we can also set up an
[initramfs](https://en.wikipedia.org/wiki/Initial_ramdisk) - we do this now, because we have defined the most
basic root file system that will be booted into (for the sake of
brevity we will not set up any kernel modules, otherwise these
should be built first and included in the initramfs).

For our initramfs we will want an `init` and `shutdown` script,
and a copy of the sysroot created previously. We start with an
initramfs-scripts element:

    # elements/image/initramfs/initramfs-scripts.bst

    kind: import

    sources:
    - kind: local
      path: files/initramfs-scripts
      directory: /sbin

This will simply place the `init` and `shutdown` scripts located
in `files/initramfs-scripts` in `/sbin`, where they can later be
found and executed.

We then define our initramfs as the intersection between our
initramfs-scripts and sysroot content:

    # elements/image/initramfs/initramfs.bst

    kind: compose
    description: Initramfs composition

    depends:
      - filename: contents.bst
        type: build
      - filename: image/initramfs/initramfs-scripts.bst
        type: build

    config:
      include:
      - runtime

Finally we create an element that produces the cpio archive and
compress it using gzip:

    # elements/image/initramfs/initramfs-gz.bst

    kind: script
    description: The compressed initramfs

    depends:
    - filename: image/initramfs/initramfs.bst
      type: build
    - filename: contents.bst
      type: build

    variables:
      cwd: "%{build-root}"

    config:
      layout:
      - element: contents.bst
        destination: /
      - element: image/initramfs/initramfs.bst
        destination: "%{build-root}"

      commands:
      - mkdir -p %{install-root}/boot
      # We need to ensure exec permissions here.
      # See: https://gitlab.com/BuildStream/buildstream/issues/84
      - chmod +x ./sbin/init ./sbin/shutdown
      - (find . -print0 | cpio -0 -H newc -o) |
        gzip -c > %{install-root}/boot/initramfs.gz

### A Linux kernel<a id="sec-1-3-3" name="sec-1-3-3"></a>

Now that our final environment is set up, we create the Linux
kernel that will drive it. Setup for this is a little less
intricate since it only involves building a single project:

    # elements/image/linux.bst

    kind: manual

    depends:
    - filename: base.bst
      type: build

    sources:
    - kind: tar
      url: kernel:linux/kernel/v4.x/linux-4.14.3.tar.xz
      ref: b628134ebb63ec82cb3e304a10aab2fd306fe79212e75ce81d8af73463c80366
    config:
      configure-commands:
      - sed -i 's|#!/bin/bash|#!/bin/ash|' scripts/config

`<numerous lines of configuration>`

      - scripts/config -m CONFIG_DRM_VIRTIO_GPU
      - scripts/config -e BT
      - yes '' | make oldconfig
      build-commands:
      - make $MAKEFLAGS
      install-commands:
      - mkdir -p "%{install-root}"/boot
      - make INSTALL_PATH="%{install-root}"/boot install
      - make INSTALL_MOD_PATH="%{install-root}" modules_install
      - install -d "%{install-root}%{prefix}/src/linux"
      - |
        (
            printf 'Makefile\0'
            printf 'Module.symvers\0'
            find arch/x86 -maxdepth 1 -name 'Makefile*' -print0
            find arch/x86 \( -name 'module.lds' -o -name 'Kbuild.platforms' -o -name 'Platform' \) -print0
            find arch/x86 \( -type d -a \( -name include -o -name scripts \) \) -o \
                                     \! -type d -a \( -path '*include/*' -o -path '*scripts/*' \) -print0
            find include -name 'asm*' -prune -o -print0
            find include/asm-generic -print0
            find include/uapi -print0
            find scripts -print0
        ) | cpio -0pumd "%{install-root}%{prefix}/src/linux"
    public:
      bst:
        integration-commands:
        - if which depmod; then (cd /usr/lib/modules && for version in *; do depmod -a
          "$version"; done) fi

The main complexity in compiling a Linux kernel is its
configuration; the 'correct' settings depend a lot on the
project. The remaining configuration should be quite portable to
other builds, however, and simply deals with placing files in the
correct locations.

### Project-specific elements<a id="sec-1-3-4" name="sec-1-3-4"></a>

Finally, our project-specific files should be included. For a
real project, this may be an installer, 'rescue' applications
such as parted, distribution-specific files or similar.

In our case, we will include a 'hello' script that simply prints
'Hello World!', as is tradition:

    # elements/contents/hello.bst

    kind: import

    sources:
      - kind: local
        path: files/hello
        directory: /usr/bin

    depends:
      - filename: contents/busybox.bst
        type: runtime

We also update the `contents.bst` file to include our project
target:

    # elements/contents.bst

    kind: stack
    description: The image contents
    depends:
      - contents/hello.bst
      - contents/busybox.bst

While our 'hello' element run-depends on busybox, our contents
**must** include a working set of coreutils - we make this explicit
by also depending on busybox.

## Creating the image<a id="sec-1-4" name="sec-1-4"></a>

Now that all image content is defined, we can create the elements
that actually build the image. The x86image plugin requires two
elements:

-   A base system that contains the tools to create an image
-   An element that contains the system we want to create an image of

We already have `base.bst`, which conveniently contains all tools
we need (we could have used a separate base system to create the
image), but we still need to create a system element.

For the system element, we simply collect the image content
elements and their runtime dependencies:

    # elements/image/system.bst

    kind: compose
    description: The system installed onto the image

    depends:
      - filename: contents.bst
        type: build
      - filename: image/linux.bst
        type: build
      - filename: image/initramfs/initramfs-gz.bst
        type: build

    config:
      include:
        - runtime

We can now define our image element - we start by depending on the
above elements:

    # elements/image-x86_64.bst

    # Specify the plugin reference - we want to import the x86image plugin
    # from buildstream-external
    kind: x86image
    description: Create a deployment of a Linux system
    depends:
      - filename: image/system.bst
        type: build
      - filename: base.bst
        type: build

We then set a few parameters to suit our system:

    # These settings are tailored to this specific project, don't just
    # blindly copy.
    variables:
      # Size of the disk to create
      #
      # Should be able to calculate this based on the space
      # used, however it must be a multiple of (63 * 512) bytes
      # as mtools wants a size that is divisible by sectors (512 bytes)
      # per track (63).
      boot-size: 16M
      rootfs-size: 69M
      sector-size: 512
      swap-size: 40K
      kernel-args: root=/dev/sda2 rootfstype=ext4 init=/sbin/init console=ttyS0
      kernel-name: vmlinuz-4.14.3

The correct values for these parameters will depend on the
specific image created, but for this project the following values
were used:

-   **boot-size:** The size of `/boot` as created by
    `image/system.bst` - the system can be inspected
    using `bst checkout`.
-   **rootfs-size:** The size of `/` as created by `image/system.bst`.
-   **sector-size:** The default size of 512 should work in most
    cases, your requirements may differ.
-   **swap-size:** The desired size for the swap partition.
-   **kernel-args:** The kernel arguments - the image plugin will by
    default create the following `/etc/fstab`:

        /dev/sda2   /       ext4   defaults,rw,noatime   0 1
        /dev/sda1   /boot   vfat   defaults              0 2
        /dev/sda3   none    swap   defaults              0 0

    Hence we specify `root=/dev/sda2` and `rootfstype=ext4`.

    `image/initramfs-scripts.bst` defines our init
    script as `/sbin/init`, hence we set
    `init=/sbin/init`.

    Finally, qemu (which we will use to try out this
    image) requires our console to be on ttyS0, so we
    specify `console=ttyS0`.
-   **kernel-name:** The default name of the kernel name as created by
    `image/linux.bst` is `vmlinuz-4.14.3`, and since
    this is easier than renaming it we specify that
    value.

The final configuration specifies which dependencies to use as the
base/system elements, and creates a script to launch our image
using qemu:

    config:
      # The element that should be staged into "/". It must contain
      # all the tools required to generate the image
      base: base.bst

      # The element that should be staged into %{build-root}. It is expected
      # to be the system that you're planning to turn into an image.
      input: image/system.bst

      # This is technically not necessary, but convenient for trying
      # things out :)
      final-commands:
        (>):
        - |
          cat > %{install-root}/run-in-qemu.sh << EOF
          #!/bin/sh
          qemu-system-x86_64 -drive file=sda.img,format=raw -nographic
          EOF
          chmod +x %{install-root}/run-in-qemu.sh

## Building the image<a id="sec-1-5" name="sec-1-5"></a>

We have now defined everything required to build a basic Linux
image. With bst and the bst-external plugin repository installed,
we can now build and boot our image.

We first run `bst track --deps all image-x86_64.bst` to determine
references for all sources used to build this project. We then run
`bst build image-x86_64.bst` to build and finally `bst checkout
   image-x86_64.bst image` to retrieve our finalized image.

To test the result, simply run `cd image/ && ./run-in-qemu.sh`
(perhaps as root), and the image should boot.
